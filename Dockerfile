FROM timbru31/node-alpine-git:20
RUN git clone https://gitlab.com/ziff_davis/angularjs-template.git && cd angularjs-template
WORKDIR /angularjs-template
RUN npm install
EXPOSE 8000
CMD ["npm", "start", "--host", "0.0.0.0"]